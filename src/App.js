import React, { Component } from 'react';
import List from 'react-smooth-draggable-list';
import { YMaps, Map, Placemark, Polyline } from 'react-yandex-maps';
import './App.css';

class App extends Component {

  // начальные параметры карты
  mapState = {center: [55.734111, 37.623914], zoom: 17, controls: ['zoomControl']};

  state = {
    // порядок точек в списке: последовательное (в соовтествии с отображаемым порядком)
    // перечисление индексов из points
    order: [],

    // массив объектов вида: {coords: [lat, long], name: ..., address: ...}
    points: [],

    // содержимое поля ввода
    inputValue: ''
  };

  constructor(props) {
    super(props);
    this.formSubmit = this.formSubmit.bind(this);
    this.deleteBtnClick = this.deleteBtnClick.bind(this);
    this.placemarkGeometryChange = this.placemarkGeometryChange.bind(this);
    this.placemarkDragEnd = this.placemarkDragEnd.bind(this);
  }

  map() {
    return (
      <YMaps ref={(ref) => this.YMapsRef = ref}>
        <Map instanceRef={(inst)=> this.mapInst = inst}
             width="100%"
             height="100%"
             state={this.mapState}
             options={{suppressMapOpenBlock: true}}
        >
          {/* ломаная */}
          <Polyline
            geometry={{coordinates: this.state.order.map(pointIndex => this.state.points[pointIndex].coords)}}
            options={{strokeWidth: 2}}
          />

          { // точки
            this.state.order.map((pointIndex, idx) => {
              const point = this.state.points[pointIndex];
              return <Placemark key={pointIndex}
                                geometry={{coordinates: point.coords}}
                                properties={{
                                  iconContent: idx + 1,
                                  balloonContentHeader: point.name,
                                  balloonContent: point.address
                                }}
                                options={{draggable: true}}
                                onGeometryChange={e => this.placemarkGeometryChange(e, idx)}
                                onDragEnd={() => this.placemarkDragEnd(pointIndex, idx)}
              />
            })
          }

        </Map>
      </YMaps>
    )
  }

  placemarkGeometryChange(e, idx) {
    // можно было бы сделать так:

    // const points = this.state.points.slice();
    // points[pointIndex].coords = e.originalEvent.originalEvent.originalEvent.newCoordinates;
    // this.setState({points});

    // но это не оптимально с точки зрения производительности. особенно заметно подтормаживает
    // при перетаскивании, когда в маршруте много точек, т.к. на каждое движение мыши
    // перерендеривается весь маршрут. вместо этого применена следующая оптимизация:
    // при перетаскивании обновляется только соотвествующий узел ломаной, состояние же
    // обновляется по завершении перетаскиявания в методе placemarkDragEnd.
    // this.mapInst.geoObjects.get(0) - ломаная
    this.mapInst.geoObjects.get(0).geometry.set(idx, e.originalEvent.originalEvent.originalEvent.newCoordinates);
  }

  placemarkDragEnd(pointIndex, idx) {
    // Не знаю корректны ли две следующие строки кода. Они напрямую изменяют состояние,
    // вернее вложенный в него объект. Перерисовка компонента здесь не требуется, это
    // корректировка координат на будущее.
    // Как вообще в таких случаях (при изменении вложенных в состояние объектов) корректно
    // поступать? Неужели создавать глубокую копию состояния, мутировать её и делать setState?
    const points = this.state.points;
    points[pointIndex].coords = this.mapInst.geoObjects.get(0).geometry.get(idx);

    this.loadAddress(pointIndex);
  }

  async loadAddress(pointIndex) {
    const points = this.state.points;
    let address;
    try {
      const res = await this.YMapsRef.state.ymaps.geocode(points[pointIndex].coords, {results: 1});
      address = res.geoObjects.get(0) ? res.geoObjects.get(0).properties.get('name') : 'Не удалось определить адрес.';
    } catch(e) {
      address = 'Ошибка при получении адреса.'
    }
    // здесь тот же вопрос о изменении объектов вложенных в состояние
    points[pointIndex].address = address;
    this.forceUpdate();
  }

  panel() {
    return (
      <div>
        <form onSubmit={this.formSubmit}>
          <input autoFocus type="text" value={this.state.inputValue}
                 onChange={e => this.setState({inputValue: e.target.value})}
                 placeholder="Новая точка маршрута"
          />
        </form>

        {
          this.state.points.length > 0 &&
          <List
            rowHeight={25}
            order={this.state.order}
            onReOrder={order => this.setState({order})}
            springConfig={{
              stiffness: 500,
              damping: 18,
              precision: 0.05
            }}
          >
            {
              this.state.points.map((item, idx) =>
                <List.Item key={idx}>
                  <div className="ellipsed">{this.state.order.indexOf(idx) + 1 + '. ' + item.name}</div>
                  <a onClick={() => this.deleteBtnClick(idx)} className="btn btn-default btn-xs">
                    <img src="/delete.png" width="18" alt="Delete" />
                  </a>
                </List.Item>
              )
            }
          </List>
        }
      </div>
    )
  }

  formSubmit(e) {
    e.preventDefault();
    if (this.state.inputValue.trim() === '')
      return;
    this.addPoint(this.state.inputValue);
    this.setState({inputValue: ''});
  }

  addPoint(name) {
    const points = this.state.points.slice();
    const order = this.state.order.slice();
    points.push({
      name,
      coords: this.mapInst.getCenter()
    });
    const index = order.length;
    order.push(index);
    this.setState({points, order});
    setTimeout(() => this.loadAddress(index), 0);
  }

  deleteBtnClick(idx) {
    const points = this.state.points.slice();
    const order = this.state.order.slice();
    points.splice(idx, 1);
    const pos = order[idx];
    order.splice(idx, 1);
    for (let i = 0; i < order.length; i++)
      if (order[i] > pos)
        order[i]--;
    this.setState({order, points});
    // в некоторых случаях после удаления точки ломанная отображается не верно.
    // видимо это какой-то глюк яндекс карт либо react-yandex-maps
    // помогает следующая строка:
    setTimeout(() => this.setState({}), 0);
  }

  render() {
    return [
      <div id="panel" key="panel">
        {this.panel()}
      </div>,
      <div id="map" key="map">
        {this.map()}
      </div>
    ];
  }
}

export default App;
